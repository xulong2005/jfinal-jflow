package BP.WF.HttpHandler.Base;
import javax.servlet.http.HttpServletRequest;
import BP.WF.HttpHandler.WF_Comm;
import BP.WF.HttpHandler.Base.HttpHandlerBase;

public class WF_Comm_Controller extends HttpHandlerBase {
	
	public WF_Comm_Controller(){
		
		System.out.println("WF_Comm_Controller 初始化.");
		
	}
	

	/**
	 * jfinal 默认执行的方法
	 */
	public void index() {
		this.ProcessRequestPost(this.getRequest());
	}
	
	/**
	 * 默认执行的方法
	 * 
	 * @return
	 */
	public final void ProcessRequestPost(HttpServletRequest request) {
		WF_Comm CommHandler = new WF_Comm();
		
		//调试用日志
//		String temp = request.getRequestURL().toString();
//		System.out.println(temp);
//
//		Enumeration enump = request.getParameterNames();
//		while(enump.hasMoreElements()){
//			String name = enump.nextElement().toString();
//			System.out.println(name + ":" + request.getParameter(name));
//		}
		//JFinal方式返回JSON数据
		String data = super.getProcessRequestResult(CommHandler);
		System.out.println(data);
		this.renderJson(data);
	}
	

	@Override
	public Class<WF_Comm> getCtrlType() {
		return WF_Comm.class;
	}

}
